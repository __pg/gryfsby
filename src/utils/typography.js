import Typography from 'typography'
import Wordpress2016 from 'typography-theme-wordpress-2016'
import elkGlenTheme from 'typography-theme-elk-glen'

Wordpress2016.overrideThemeStyles = () => ({
  'a.gatsby-resp-image-link': {
    boxShadow: 'none',
  },
})

elkGlenTheme.overrideThemeStyles = () => ({
  'a': {
    backgroundImage: 'none',
  },
})

const typography = new Typography(elkGlenTheme)

// Hot reload typography in development.
if (process.env.NODE_ENV !== 'production') {
  typography.injectStyles()
}

export default typography
