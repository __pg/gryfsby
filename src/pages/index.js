import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'

import { rhythm } from '../utils/typography'
import logoPic from './logo.png'
import bgPic from './pw_maze_black_2X.png'

class SiteIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const textStyle = {  
            padding: '15px',
            color: '#bcbcbc',
            fontSize: '14px',
            fontWeight: '400',
            fontFamily: 'Montserrat, sans-serif',
            display: 'inline-block',
            borderRadius: '3px',
            textTransform: 'uppercase',
            lineHeight: '25px',
            marginBottom: '20px',
            transition: 'all 0.3s ease-in-out',
            textShadow: 'none'
          }
    return <div style={{}}>
              <Helmet title={siteTitle}>
                <style type="text/css">
                  {`body {background-image: url(` + bgPic + `);}`}
                </style>
              </Helmet>
              <div class="container" style={{ textAlign: 'center' }}>
                <div class="logo animated fadeInDown delay-07s">
                  <a href="#">
                    <img src={logoPic} alt={`Gryfus`} style={{ marginBottom: 0, width: '130px' }} />
                  </a>
                </div>
                <div class="we-create animated fadeInUp delay-1s">
                  <p style={textStyle}>
                    Jesteśmy grupą pasjonatów tworzących wyjątkowe
                    piwa.
                  </p>
                </div>
                <Link
                  style={Object.assign(
                    {},
                    textStyle,
                    { background: '#7cc576', color: '#fff' }
                  )}
                  to="news">
                  JESTEM PEŁNOLETNI(A){' '}
                </Link>
              </div>
            </div>
      }
}

export default SiteIndex
