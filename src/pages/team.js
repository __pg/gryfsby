import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'

import Bio from '../components/Bio'
import { rhythm } from '../utils/typography'

class TeamIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const posts = get(this, 'props.data.allMarkdownRemark.edges')

    return <div>
        <Helmet title={siteTitle + ' | O Nas'} />
        <div style={{ textAlign: 'center', paddingTop: '30px' }}>
          <h2
            style={{
              fontSize: '34px',
              color: '#222222',
              fontFamily: "'Montserrat', sans-serif",
              fontWeight: '700',
              letterSpacing: '-1px',
              margin: '0 0 15px 0',
              textAlign: 'center',
              textTransform: 'uppercase',
            }}
          >
            O Nas
          </h2>
          <p style={{ fontSize: '18px', color: '#888' }}>
            Dzielimy się wiedzą i doświadczeniem w przyjaznej i twórczej
            atmosferze.
          </p>
        </div>
        {posts.map(({ node }) => {
          const title = get(node, 'frontmatter.title') || node.fields.slug
          return <div key={node.fields.slug}>
              <h3 style={{ marginBottom: rhythm(1 / 4) }}>
                <Link style={{ boxShadow: 'none' }} to={node.fields.slug}>
                  {title}
                </Link>
              </h3>
              <small>{node.frontmatter.date}</small>
              <p dangerouslySetInnerHTML={{ __html: node.excerpt }} />
            </div>
        })}
      </div>
  }
}

export default TeamIndex

export const teamQuery = graphql`
  query TeamQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "DD MMMM, YYYY")
            title
          }
        }
      }
    }
  }
`
