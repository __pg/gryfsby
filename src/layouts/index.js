import React from 'react'
import Link from 'gatsby-link'
import { Container } from 'react-responsive-grid'
import Helmet from 'react-helmet'

import { rhythm, scale } from '../utils/typography'
import smallLogo from './small-logo.png'

class Template extends React.Component {
  render() {
    const { location, children } = this.props
    let header
    let section

    let rootPath = `/`
    if (typeof __PREFIX_PATHS__ !== `undefined` && __PREFIX_PATHS__) {
      rootPath = __PATH_PREFIX__ + `/`
    }
    let link = (
      <Link
        style={{
          boxShadow: 'none',
          textDecoration: 'none',
          color: 'inherit',
        }}
        to={'/'}
      >
        Browar Gryfus
      </Link>
    )
    let isRoot = location.pathname === rootPath
    if (isRoot ) {
      header = null
      section = (
        <div style={{marginTop:"50px"}}>
        {children()}
        </div>
      )
    } else {
      const liStyle = {display:"inline", margin:"0px 0px"}
      const liAStyle = {
        display: "inline-block",
        color: "#222222",
        textTtransform: "uppercase",
        fontFamily: "'Montserrat', sans-serif",
        textDecoration: "none",
        lineHeight: "20px",
        margin: "17px 35px",
        transition: "all 0.3s ease-in-out",
        verticalAlign: "middle"
      }
      header = (
        <ul style={{
          textAlign: "center",
          margin: "10px 10px 0px",
          padding: "0px",
          listStyle: "none",
        }}>
            <li style={liStyle} ><a style={liAStyle} href="/news">Nowości</a></li>
            <li style={liStyle} ><a style={liAStyle} href="/team">O Nas</a></li>
            <li style={liStyle} ><a style={liAStyle} href="/service">Usługi</a></li>
            <li style={liStyle}  class="small-logo"><a style={liAStyle} href="/"><img style={{width:"50px"}} src={smallLogo} alt=""/></a></li>
            <li style={liStyle} ><a style={liAStyle} href="/portfolio">Portfolio</a></li>
            <li style={liStyle} ><a style={liAStyle} href="/galeria">Galeria</a></li>
            <li style={liStyle} ><a style={liAStyle} href="/contact">Kontakt</a></li>
        </ul>
      )
      section = (<section style={{ maxWidth: '960px', marginLeft: 'auto', marginRight: 'auto', marginTop: "150px" }}>
          {children()}
        </section>
      )
    }
      return <Container style={{ margin: '0px', maxWidth: '100%', paddingLeft: '0px', paddingEight: '0px' }}>
          <Helmet>
            <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css' />
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css' />
          </Helmet>
          <nav
            style={{
              zIndex: '1000',
              position: 'fixed',
              top: '0px',
              width: '100%',
              left: '0px',
              marginLeft: '0px',
              padding: '0px',
              borderBottom: '1px solid #dddddd',
              boxShadow: '0 4px 5px -3px #ececec',
              background: '#fff',
              fontSize:"14px",
              textTransform: "uppercase",
            }}
          >
            {header}
          </nav>
          {section}
        </Container>
  }
}

export default Template
